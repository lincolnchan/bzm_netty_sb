
// 时间格式化	
function commonDateFormat(datetime,fmt) {		
	  if(!datetime){
		 return "-"; 
	  }	
	  if (parseInt(datetime)==datetime) {
	    if (datetime.length==10) {
	      datetime=parseInt(datetime)*1000;//时间戳为10位需*1000，时间戳为13位的话不需乘1000
	    } else if(datetime.length==13) {
	      datetime=parseInt(datetime);
	    }
	  }
	  datetime=new Date(datetime);
	  var o = {
	  "M+" : datetime.getMonth()+1,                 //月份   
	  "d+" : datetime.getDate(),                    //日   
	  "h+" : datetime.getHours(),                   //小时   
	  "m+" : datetime.getMinutes(),                 //分   
	  "s+" : datetime.getSeconds(),                 //秒   
	  "q+" : Math.floor((datetime.getMonth()+3)/3), //季度 	  
	  "S"  : datetime.getMilliseconds()             //毫秒   
	  };   
	  if(/(y+)/.test(fmt))   
	  fmt=fmt.replace(RegExp.$1, (datetime.getFullYear()+"").substr(4 - RegExp.$1.length));   
	  for(var k in o)   
	  if(new RegExp("("+ k +")").test(fmt))   
	  fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));   
	  return fmt;
}

// radio 选中
function radioTyleChecked(t) {				
	$('.radio-icon').removeClass('checked');	
	$(t).parent().find('.radio-icon').addClass('checked');
}
	
// 控制只能输入数字	
function numberFilter(obj){			
	obj.value = obj.value.replace(/\D/g,"");
}	

// 控制职能输入英文
function englishFilter(obj){			
	obj.value = obj.value.replace(/[^\w\.\/]/ig,'');
}

// 控制金额输入	
function moneyFilter(obj){    
    obj.value = obj.value.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符     
    obj.value = obj.value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的     
    obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");    
    obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');//只能输入两个小数     
    if(obj.value.indexOf(".")< 0 && obj.value !=""){//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额    
    	obj.value= parseFloat(obj.value);    
    }   	 
}
// 控制尺寸范围输入	小数点后最多留4位
function otherMoneyFilter(obj){    
	obj.value = obj.value.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符     
	obj.value = obj.value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的     
	obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");    
	obj.value = obj.value.replace(/^(\d*(\.\d{0,5})?).*/,'$1');//只能输入4个小数     
	if(obj.value.indexOf(".")< 0 && obj.value !=""){//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额    
		obj.value= parseFloat(obj.value);    
	}   	 
}

// 只能输入 数字  和  英文 
function numberEnglishFilter(obj){			
	obj.value = obj.value.replace(/(?!^\d+$)(?!^[a-zA-Z]+$)[0-9a-zA-Z]{4,23}/,"");
}	

// 显示手机号
function showSjInfo(val) {
	if(!val||val==null||val=="null"){
		val = "";
	}
	layer.open({
	  type: 1,
	  skin: 'layui-layer-rim', //加上边框
	  area: ['420px', '240px'], //宽高
	  content: '手机号：'+val	
	});
}

// 显示qq号
function showQQInfo(val) {
	if(!val||val==null){
		val = "";
	}
	layer.open({
	  type: 1,
	  skin: 'layui-layer-rim', //加上边框
	  area: ['420px', '240px'], //宽高
	  content: 'QQ号：'+val
	});
}

// 显示微信号
function showWXInfo(val) {
	if(!val||val==null){
		val = "";
	}	
	layer.open({
	  type: 1,
	  skin: 'layui-layer-rim', //加上边框
	  area: ['420px', '240px'], //宽高
	  content: '微信号：'+val
	});
}
	
// js控制千分位	
function formatMoneyNum(num){  
   if(num){	
		return num.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');	
   }
   return '0.00';  
}  

function checkPhone(phone){ 
    if(!(/^1[345789]\d{9}$/.test(phone))){ 
        //alert("手机号码格式有误，请重填");  
        return false; 
    }else{	
		return true; 
	} 
}

//日期加天数的方法
//dataStr日期字符串
//dayCount 要增加的天数
//return 增加n天后的日期字符串
function dateAddDays(dataStr,dayCount) {
	if(!dayCount||dayCount=="null"){
		return dataStr;	
	}		
	var strdate=dataStr; //日期字符串
	var isdate = new Date(strdate.replace(/-/g,"/"));  //把日期字符串转换成日期格式
	isdate = new Date((isdate/1000+(86400*dayCount))*1000);  //日期加1天
	//var pdate = isdate.getFullYear()+"-"+(isdate.getMonth()+1)+"-"+(isdate.getDate());   //把日期格式转换成字符串
	var pdate = new Date();
	if(isdate.getMonth()<9){
		var pdate = isdate.getFullYear()+"-0"+(isdate.getMonth()+1)+"-"+(isdate.getDate());   //把日期格式转换成字符串
		if(isdate.getDate()<9){
			var pdate = isdate.getFullYear()+"-0"+(isdate.getMonth()+1)+"-0"+(isdate.getDate());   //把日期格式转换成字符串
		}else{
			var pdate = isdate.getFullYear()+"-0"+(isdate.getMonth()+1)+"-"+(isdate.getDate());   //把日期格式转换成字符串
		}
	}else{
		var pdate = isdate.getFullYear()+"-"+(isdate.getMonth()+1)+"-"+(isdate.getDate());   //把日期格式转换成字符串
	}
	return pdate;
}

// 获得注册类型	注册类型    employee  员工  、     system 系统 、    shop  店铺	、  supplier  供应商 、  member 会员
function getRegisterTypeMsg(type){
	var html = "";
	if(type=="employee"){
		html = '<p>员工</p>';
		
	}else if(type=="system"){
		html = '<p>系统</p>';
		
	}else if(type=="shop"){
		html = '<p>店铺</p>';
		
	}else if(type=="supplier"){
		html = '<p>供应商</p>';
		
	}else if(type=="member"){
		html = '<p>会员</p>';
		
	}else{
		html = '<p>-</p>';
	}
	return html;
}

//预览图片	
var previewRotate = 0;
// picture-preview-btn
$(document).on('click', '.picture-preview-btn', function() {
	var url = $(this).attr("src")
	previewRotate = 0;	
	previewImg(url);
})

// 预览 图片
function previewImg(url) {
    var img = new Image();  	
    img.src = url;	
    var imgHtml = "";			
    imgHtml += '<div style="text-align: center;">';	
   	imgHtml += '<img class="preview-img" src=' + url + ' width="95%;" style="transform:rotate('+previewRotate+'deg);margin-top: 20px;max-width: 400px;" />';	
   	imgHtml += '</div>';	
   	imgHtml += '<div style="position: absolute; bottom: 20px; text-align: center; width: 100%;">';
    imgHtml += '<button type="button" class="layui-btn layui-btn-normal layui-btn-sm left-rotate90">左旋转90</button>';
    imgHtml += '<button type="button" class="layui-btn layui-btn-normal layui-btn-sm right-rotate90">右旋转90</button>';
	imgHtml += '</div>';
    //弹出层
    layer.open({  
        type: 1,  
        shade: 0.8,
        offset: 'auto',
        area: [550 + 'px',700+'px'],
        shadeClose:true,
        scrollbar: false,
        title: "图片预览", //不显示标题  	
        content: imgHtml, //捕获的元素，注意：最好该指定的元素要存放在body最外层，否则可能被其它的相对元素所影响  
        cancel: function () {  
            //layer.msg('捕获就是从页面已经存在的元素上，包裹layer的结构', { time: 5000, icon: 6 });  
        }  
    }); 
}

// 预览左旋转90
$(document).on('click', '.left-rotate90', function() {
	previewRotate = previewRotate - 90;	
	$(".preview-img").css('transform', 'rotate(' + previewRotate + 'deg)');
})


// 预览又旋转90
$(document).on('click', '.right-rotate90', function() {
	previewRotate = previewRotate + 90;	
	$(".preview-img").css('transform', 'rotate(' + previewRotate + 'deg)');
})

/**
 * 获取子节点，所有父节点的name的拼接字符串
 * @param treeObj
 * @returns
 */
function getCommonZtreeFilePath(treeObj){
	if(treeObj==null){
		return "";
	}				
	var filename = treeObj.name;
	var pNode = treeObj.getParentNode();
	if(pNode!=null){
	filename = getCommonZtreeFilePath(pNode) +" > "+ filename;
	}		
	return filename;
}

/**
 * 获得  用户类型    超级管理员   1000、员工   1001、普通用户    1002
 * @param type
 * @returns
 */
function getCommonUserType(type){
	var html = "";
	if(type==1000){
		html = '<p>超级管理员</p>';	
	}else if(type==1001){
		html = '<p>员工</p>';
	}else if(type==1002){
		html = '<p>普通用户</p>';
	}else{
		html = '<p>-</p>';
	}
	return html;
}

/**
 * 获得用户状态
 * @param status
 * @returns
 */
function getCommonUserStatus(status){
	var html = "";
	if(status==1){	
		html = '<p style="color: #5fb878;">正常</p>';	
	}else if(status==2){
		html = '<p style="color: red;">停用</p>';
	}else if(status==3){
		html = '<p style="color: red;">冻结</p>';
	}else{
		html = '<p>-</p>';
	}	
	return html;
}

/**
 * 获得随机数
 * @param number
 * @returns
 */
function getCommonMathRand(number) { 
	if(!number||number==0){
			number = 6
	}
	var Num=""; 
	for(var i=0;i<number;i++) { 
			Num+=Math.floor(Math.random()*10); 
	}
	return Num;
} 

/**
 * 商品状态  1、上架中   2、下架中  3、系统下架  4、草稿箱  5、已删除
 * @returns
 */
function getCommonProductStatus(status) {
	var html = "";
	if(status==1){	
		html = '<p style="color: #5fb878;">上架中</p>';	
	}else if(status==2){
		html = '<p style="color: red;">下架中</p>';
	}else if(status==3){
		html = '<p style="color: #e6c40e;">系统下架</p>';
	}else if(status==4){
		html = '<p>草稿箱</p>';
	}else if(status==5){
		html = '<p style="color: red;">已删除</p>';
	}else{
		html = '<p>-</p>';
	}	
	return html;
}











