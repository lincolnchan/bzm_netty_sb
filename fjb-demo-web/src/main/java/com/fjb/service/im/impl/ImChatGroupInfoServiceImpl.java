package com.fjb.service.im.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fjb.mapper.im.ImChatGroupInfoMapper;
import com.fjb.pojo.im.ImChatGroupInfo;
import com.fjb.service.im.ImChatGroupInfoService;

/**
 * <p>
 * im_chat聊天组信息 服务实现类
 * </p>
 *
 * @author hemiao
 * @since 2020-06-02
 */
@Service
public class ImChatGroupInfoServiceImpl extends ServiceImpl<ImChatGroupInfoMapper, ImChatGroupInfo> implements ImChatGroupInfoService {

}
