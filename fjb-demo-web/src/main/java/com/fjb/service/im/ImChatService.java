package com.fjb.service.im;

/**
 * @Description:即时通讯聊天接口
 * @author hemiao
 * @time:2020年5月19日 下午9:44:02
 */
public interface ImChatService {

	// 获得连接
	Object getConnect(Object obj);
	
	// 发送信息
	Object sendMsg(Object obj);
	
	// 接收信息
	Object receiveMsg(Object obj);
	
	// 退出
	Object setQuit(Object obj);
}
