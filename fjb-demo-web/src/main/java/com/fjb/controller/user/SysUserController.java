package com.fjb.controller.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fjb.entity.HttpCode;
import com.fjb.entity.JsonResult;
import com.fjb.pojo.user.SysUser;
import com.fjb.service.user.SysUserService;

/**
 * @Description:TODO
 * @author hemiao
 * @time:2020年6月1日 下午10:22:34
 */
@Controller
@RequestMapping("/sysUser")
public class SysUserController {
	
	@Autowired
	private SysUserService sysUserService;
	
	/**
	 * @Description:获得所有用户列表
	 * @param request
	 * @return
	 * JsonResult<List<SysUser>>
	 * @exception:
	 * @author: hemiao
	 * @time:2020年6月1日 下午10:28:58
	 */
	@RequestMapping("/selectAllList")
	@ResponseBody
	public JsonResult<List<SysUser>> selectAllList(HttpServletRequest request){
		QueryWrapper<SysUser> queryWrapper = new QueryWrapper<SysUser>();
		List<SysUser> infoList = sysUserService.list(queryWrapper);
		return new JsonResult<List<SysUser>>(infoList, HttpCode.SUCCESS);
	}
}
