package com.fjb.pojo.user;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 用户信息
 * </p>
 *
 * @author hemiao
 * @since 2020-02-16
 */
public class SysUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 主账号id	 
     */
    private Integer mainUserId;

    /**
     * 用户头像
     */
    private String pictureUrl;

    /**
     *  用户类型    超级官员   1000、员工   1001、普通用户    1002
     */
    private Integer userType;

    /**
     * 性别    男    ;  女
     */
    private String gender;

    /**
     * 生日	年月日
     */
    private LocalDate birthday;

    /**
     * 用户名		最长15位 最短8位
     */
    private String username;

    /**
     * 密码    最长25位 最短8位
     */
    private String password;

    /**
     * 手机号
     */
    private String phone;

    /**
     * qq
     */
    private String qq;

    /**
     * 微信
     */
    private String wechat;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 国家名族
     */
    private String nation;

    /**
     * 省份
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 区
     */
    private String district;

    /**
     * 街道
     */
    private String street;

    /**
     * 地址
     */
    private String address;

    /**
     * 用户状态   1、正常     2、停用  
     */
    private Integer userStatus;

    private Integer createUserId;

    private LocalDateTime createTime;

    private Integer updateUserId;

    private LocalDateTime updateTime;

    /**
     * 注册来源   电脑端 windos ，微信小程序 wx_applet，app  	
     */
    private String registerSource;

    /**
     * 注册码      注册使用的邀请码
     */
    private String registerCode;

    /**
     * 邀请码   （用来邀请别人）
     */
    private String invitationCode;

    /**
     * 个人说明
     */
    private String description;

    /**
     * 微信openId
     */
    private String wxOpenId;

    /**
     * 微信登录会话KEY
     */
    private String wxSessionKey;

    /**
     * 纬度  经纬度范围是-180~180
     */
    private BigDecimal latitude;

    /**
     * 经度  经纬度范围是-180~180
     */
    private BigDecimal longitude;

    /**
     * 获得 经度 和 纬度 最新时间
     */
    private LocalDateTime locationUpdateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getMainUserId() {
        return mainUserId;
    }

    public void setMainUserId(Integer mainUserId) {
        this.mainUserId = mainUserId;
    }
    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }
    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }
    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }
    public Integer getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Integer createUserId) {
        this.createUserId = createUserId;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public Integer getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Integer updateUserId) {
        this.updateUserId = updateUserId;
    }
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
    public String getRegisterSource() {
        return registerSource;
    }

    public void setRegisterSource(String registerSource) {
        this.registerSource = registerSource;
    }
    public String getRegisterCode() {
        return registerCode;
    }

    public void setRegisterCode(String registerCode) {
        this.registerCode = registerCode;
    }
    public String getInvitationCode() {
        return invitationCode;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getWxOpenId() {
        return wxOpenId;
    }

    public void setWxOpenId(String wxOpenId) {
        this.wxOpenId = wxOpenId;
    }
    public String getWxSessionKey() {
        return wxSessionKey;
    }

    public void setWxSessionKey(String wxSessionKey) {
        this.wxSessionKey = wxSessionKey;
    }
    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }
    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }
    public LocalDateTime getLocationUpdateTime() {
        return locationUpdateTime;
    }

    public void setLocationUpdateTime(LocalDateTime locationUpdateTime) {
        this.locationUpdateTime = locationUpdateTime;
    }

    @Override
    public String toString() {
        return "SysUser{" +
        "id=" + id +
        ", mainUserId=" + mainUserId +
        ", pictureUrl=" + pictureUrl +
        ", userType=" + userType +
        ", gender=" + gender +
        ", birthday=" + birthday +
        ", username=" + username +
        ", password=" + password +
        ", phone=" + phone +
        ", qq=" + qq +
        ", wechat=" + wechat +
        ", email=" + email +
        ", nickname=" + nickname +
        ", nation=" + nation +
        ", province=" + province +
        ", city=" + city +
        ", district=" + district +
        ", street=" + street +
        ", address=" + address +
        ", userStatus=" + userStatus +
        ", createUserId=" + createUserId +
        ", createTime=" + createTime +
        ", updateUserId=" + updateUserId +
        ", updateTime=" + updateTime +
        ", registerSource=" + registerSource +
        ", registerCode=" + registerCode +
        ", invitationCode=" + invitationCode +
        ", description=" + description +
        ", wxOpenId=" + wxOpenId +
        ", wxSessionKey=" + wxSessionKey +
        ", latitude=" + latitude +
        ", longitude=" + longitude +
        ", locationUpdateTime=" + locationUpdateTime +
        "}";
    }
}
