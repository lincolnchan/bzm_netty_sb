package com.fjb.tool.im.service.handler;

import java.net.SocketAddress;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;

/**
 * @Description:出站处理程序适配器
 * @author hemiao
 * @time:2020年4月3日 下午1:59:37
 */
public class MyServerOutboundHandler extends ChannelOutboundHandlerAdapter{

	@Override
	public void bind(ChannelHandlerContext ctx, SocketAddress localAddress, ChannelPromise promise) throws Exception {
		System.out.println("Outbound bind server绑定");
	}

	@Override
	public void connect(ChannelHandlerContext ctx, SocketAddress remoteAddress, SocketAddress localAddress,
			ChannelPromise promise) throws Exception {
		System.out.println("Outbound connect server连接");
	}

	@Override
	public void disconnect(ChannelHandlerContext ctx, ChannelPromise promise) throws Exception {
		System.out.println("Outbound disconnect server断开连接");
	}

	@Override
	public void close(ChannelHandlerContext ctx, ChannelPromise promise) throws Exception {
		System.out.println("Outbound close server关闭");
	}

	@Override
	public void deregister(ChannelHandlerContext ctx, ChannelPromise promise) throws Exception {
		System.out.println(" deregister server注销");
	}

	@Override
	public void read(ChannelHandlerContext ctx) throws Exception {
		System.out.println(" read server读");
	}

	@Override
	public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
		System.out.println(" write server写");
	}

	@Override
	public void flush(ChannelHandlerContext ctx) throws Exception {
		System.out.println(" flush server刷新");
	}

}
