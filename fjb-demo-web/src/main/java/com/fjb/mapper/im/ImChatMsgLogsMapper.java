package com.fjb.mapper.im;

import com.fjb.pojo.im.ImChatMsgLogs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * im_chat聊天记录 Mapper 接口
 * </p>
 *
 * @author hemiao
 * @since 2020-06-02
 */
public interface ImChatMsgLogsMapper extends BaseMapper<ImChatMsgLogs> {

}
