package com.fjb.mapper.im;

import com.fjb.pojo.im.ImChatFriendRequestLogs;
import com.fjb.pojo.im.vo.ImChatFriendRequestLogsVo;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 好友请求记录 Mapper 接口
 * </p>
 *
 * @author hemiao
 * @since 2020-06-02
 */
public interface ImChatFriendRequestLogsMapper extends BaseMapper<ImChatFriendRequestLogs> {
	
	/**
	 * @Description:查询好友请求记录
	 * @param userId
	 * @return
	 * List<ImChatFriendRequestLogsVo>
	 * @exception:
	 * @author: hemiao
	 * @time:2020年6月3日 下午7:30:19
	 */
	List<ImChatFriendRequestLogsVo> selectRequestLogsList(@Param("userId")Integer userId);

}
